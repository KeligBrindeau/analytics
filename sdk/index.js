import axios from 'axios';
import sha1 from 'js-sha1';

let clickQueue = [];
let mouseQueue = [];

export const handleClick = (el, page, idel) => {
    el.addEventListener('click', (event) => {
        const click = { 
            element: idel, 
            page: page 
        };
        clickQueue.push(click);
    });
}

function generateUniqueId(timestamp) {
    const hash = sha1(timestamp.toString());
    const uniqueId = hash.substring(0, 8); 
  
    return uniqueId;
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
        return parts.pop().split(";").shift();
    }
}

function setCookie(name, value, hours) {
    var expires = "";
    if (hours) {
        var date = new Date();
        date.setTime(date.getTime() + (hours * 60 * 60 * 1000));
        expires = "; expires=" + date.toString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

var uniqueId = getCookie('uniqueId');


export async function handleOs() {

    if(uniqueId === undefined){
        var OSName = "Unknown OS";
        var userAgent = window.navigator.userAgent;
    
        if (userAgent.indexOf("Win") !== -1) {
            OSName = "Windows";
        } else if (userAgent.indexOf("Mac") !== -1) {
            OSName = "MacOS";
        } else if (userAgent.indexOf("Linux") !== -1) {
            OSName = "Linux";
        } else if (/iPad|iPhone|iPod/.test(userAgent)) {
            OSName = "iOS";
        } else if (/Android/.test(userAgent)) {
            OSName = "Android";
        }

        const os = {
            osType: OSName
        };

        uniqueId = generateUniqueId(new Date().getTime());
        setCookie('uniqueId', uniqueId, 1);

        await axios.post('http://localhost:8080/save-os', os)
        .then(response => {
            
        })
        .catch(error => {
        console.error('Erreur lors de l\'envoie des données:', error);
        });
    }
}


export async function handleSurvey(ratingValue){
    await axios.post('http://localhost:8080/save-survey', {
      rate: ratingValue,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });
}

 

async function saveClick(){

    const jsontab = JSON.stringify(clickQueue);

    if(clickQueue.length > 0){
      await axios.post('http://localhost:8080/save-click', jsontab)
      .then(response => {
        clickQueue = [];
      })
      .catch(error => {
      console.error('Erreur lors de l\'envoie des données:', error);
      });
    }
}


let visitRecorded = false
var cookieVisit = getCookie('cookieVisit');

let inactivityTimeout;

// var startVisit;

function resetInactivityTimer(page, startVisit) {
    clearTimeout(inactivityTimeout);
    inactivityTimeout = setTimeout(handleInactivity(page, startVisit), 900000);
}

function handleInactivity(page, startVisit) {
    if(visitRecorded === false && startVisit){
      saveVisit(page, startVisit)
    }    
}

// export async function handleVisit(){
//     if(cookieVisit === undefined){
//         cookieVisit = generateUniqueId(new Date().getTime());
//         setCookie('cookieVisit', cookieVisit, 1);
//         startVisit = Date.now();
//     }
// }

export async function setVisitListener(document, window, page){

    if(cookieVisit === undefined){
        cookieVisit = generateUniqueId(new Date().getTime());
        setCookie('cookieVisit', cookieVisit, 1);
        var startVisit = Date.now();
    }

    console.log(startVisit);
    alert("set" + startVisit);

    document.addEventListener('mousemove', resetInactivityTimer(page, startVisit));
    document.addEventListener('click', resetInactivityTimer(page, startVisit));

    window.addEventListener("beforeunload", (event) => {

        if (startVisit && visitRecorded === false) {
            alert("set", startVisit);
          saveVisit(page, startVisit);
        }
    })
}


async function saveVisit(page, startVisit){
    alert("save" + startVisit);
    const endTime = Date.now();
    alert("endtime" + typeof(endTime));
    const visitDuration = Math.round((endTime - startVisit) / 1000);
    alert("visitDuration" + visitDuration);
    visitRecorded = true;

    axios.post('http://localhost:8080/save-visit',{
      page: page,
      visitDuration: visitDuration,
    })
    .then(response => {
    })
    .catch(error => {
      console.error('Erreur lors de l\'envoi des données:', error);
    });
}


var startPositionX = null;
var startPositionY = null;
var startTime = null;

export async function handleMouseEvent(document, page){
    document.addEventListener('mousemove', (event) => {
        const { clientX, clientY } = event;
        // const type = "mousemove";
    
        if (startPositionX === null && startPositionY === null) {
          // Premier mouvement de souris
          startPositionX = clientX;
          startPositionY = clientY;
          startTime = new Date().getTime();
        }
    
        const endPositionX = clientX;
        const endPositionY = clientY;
    
        const distanceThreshold = 40; 
        const distance = Math.sqrt(
          Math.pow(endPositionX - startPositionX, 2) +
          Math.pow(endPositionY - startPositionY, 2)
        );
    
        if (distance >= distanceThreshold) {
          const endTime = new Date().getTime();
          const duration = endTime - (startTime);
    
          
          if (duration < 200 || duration > 10000) {
            return;
          }
    
          addMouseMovementToQueue(clientX, clientY, page);
      
          startPositionX = null;
          startPositionY = null;
          startTime = null;
          
        }
    });
}


function addMouseMovementToQueue(x, y, page) {
    const mouseMovement = {
      mouse_x: x,
      mouse_y: y,
      page: page,
      
    };
    mouseQueue.push(mouseMovement);
}


function saveMouseEvent(){

  const jsonData = JSON.stringify(mouseQueue);

    if(mouseQueue.length > 0){
      axios.post('http://localhost:8080/save-mouse', jsonData)
      .then(response => {
        mouseQueue = [];
      })
      .catch(error => {
        console.error('Erreur lors de la récupération des utilisateurs:', error);
      });
    }
}

setInterval(saveMouseEvent, 30000);


setInterval(saveClick, 7000);