const path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'sdk.bundle.js',
    library: 'sdk-analytics',
    libraryTarget: 'umd',
    globalObject: 'this',
  },
};