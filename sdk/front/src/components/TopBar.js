import React from "react";
import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";
import SaveIcon from "@mui/icons-material/Save";
import AddList from "./AddList";
import { styled } from '@mui/system';

const RootCard = styled(Card)(({ theme }) => ({
    padding: theme.spacing(1),
    width: "100%",
    display: "flex",
    justifyContent: "flex-end"
}));

export default function TopBar({
    onLayoutSave,
    items,
    onRemoveItem,
    onAddItem,
    originalItems
}) {
    return (
        <RootCard>
            <AddList
                items={items}
                onRemoveItem={onRemoveItem}
                onAddItem={onAddItem}
                originalItems={originalItems}
            />
            <IconButton aria-label="save" onClick={onLayoutSave}>
                <SaveIcon />
            </IconButton>
        </RootCard>
    );
}
