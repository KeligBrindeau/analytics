import React from "react";
import { styled } from '@mui/system';
import { Card, IconButton, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const StyledCard = styled(Card)(({ theme }) => ({
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column"
}));

const StyledHeader = styled('div')(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    padding: "0.5rem"
}));

const StyledSpacer = styled('div')(({ theme }) => ({
    flexGrow: 1
}));

const StyledBody = styled('div')(({ theme }) => ({
    padding: "0.5rem",
    flexGrow: 1
}));

const widgetNames = {
    a: 'Line Chart',
    b: 'Area Chart',
    c: 'Bar Chart',
    d: 'Scatter Chart',
};

export default function Widget({ id, onRemoveItem, component: Item }) {
    return (
        <StyledCard>
            <StyledHeader>
                <Typography variant="h6" gutterBottom>
                    {widgetNames[id]}
                </Typography>
                <StyledSpacer />
                <IconButton aria-label="delete" onClick={() => onRemoveItem(id)}>
                    <CloseIcon fontSize="small" />
                </IconButton>
            </StyledHeader>
            <StyledBody>
                <Item />
            </StyledBody>
        </StyledCard>
    );
}
