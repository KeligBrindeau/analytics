import React from "react";
import { AppBar, Toolbar, Typography, IconButton, Badge } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { styled } from '@mui/system';
import { Link } from 'react-router-dom';

const StyledAppBar = styled(AppBar)(({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
    [theme.breakpoints.up("sm")]: {
        zIndex: theme.zIndex.drawer + 1
    }
}));

const StyledIconButton = styled(IconButton)(({ theme }) => ({
    marginLeft: theme.spacing(0.5)
}));

const StyledSpacer = styled('div')(({ theme }) => ({
    flexGrow: 1
}));

export default function Header({
    handleDrawerToggle,
    toggleDarkMode,
    darkMode
}) {
    return (
        <StyledAppBar position="fixed">
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerToggle}
                    edge="start"
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap>
                    SDK Analytics
                </Typography>
                <StyledSpacer />
                <StyledIconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={toggleDarkMode}
                    edge="start"
                >
                    {darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
                </StyledIconButton>

                <StyledIconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                >
                    <Badge badgeContent={4} color="secondary">
                        <NotificationsIcon />
                    </Badge>
                </StyledIconButton>
                <Link to="/login" style={{color: 'white'}}>
                    <StyledIconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                    >
                        <AccountCircleIcon />
                    </StyledIconButton>
                </Link>
            </Toolbar>
        </StyledAppBar>
    );
}
