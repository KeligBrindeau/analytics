import React, { useState } from "react";
import { CssBaseline, ThemeProvider, createTheme } from "@mui/material";
import { Outlet } from "react-router-dom";
import Header from "./Header";
import Sidebar from "./Sidebar";
import { styled } from "@mui/system";
const StyledContent = styled("div")(({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: open ? 240 : 0,
    overflowX: "hidden",
    [theme.breakpoints.up("sm")]: {
        marginLeft: 0
    }
}));

const StyledRoot = styled("div")({
    display: "flex",
    minHeight: "100vh",
    backgroundColor: "#121212"
});

function Layout() {
    const [open, setOpen] = useState(true);
    const [darkMode, setDarkMode] = useState(true);

    const handleDrawerToggle = () => {
        setOpen(!open);
    };

    const toggleDarkMode = () => {
        setDarkMode(!darkMode);
    };

    const theme = createTheme({
        palette: {
            mode: darkMode ? "dark" : "light"
        }
    });

    return (
        <ThemeProvider theme={theme}>
            <StyledRoot>
                <CssBaseline />
                <Header handleDrawerToggle={handleDrawerToggle} />
                <Sidebar open={open} handleDrawerToggle={handleDrawerToggle} />
                <StyledContent open={open}>
                    <Outlet />
                </StyledContent>
            </StyledRoot>
        </ThemeProvider>
    );
}

export default Layout;
