import { useContext, useEffect, useRef, useState } from 'react';
import axios from 'axios';
import "../pages/kpi.css";
import HeatMap from 'heatmap-ts'
import Chart from 'chart.js/auto';
import { Responsive as ResponsiveGridLayout } from "react-grid-layout";

const initialLayouts = {
    lg: [
        { w: 6, h: 6, x: 0, y: 0, i: "a", moved: false, static: false },
        { w: 3, h: 6, x: 9, y: 0, i: "b", moved: false, static: false },
        { w: 3, h: 6, x: 6, y: 0, i: "c", moved: false, static: false },
        { w: 12, h: 4, x: 0, y: 6, i: "d", moved: false, static: false }
    ]
};
export function Kpi() {
    const [layouts, setLayouts] = useState(
        getFromLS("layouts") || initialLayouts
    );
    const onLayoutChange = (_, allLayouts) => {
        setLayouts(allLayouts);
    };
    const [heatMapData, setHeatmapData] = useState([]);
    const [clickData, setClickData] = useState([]);
    const [osData, setOsData] = useState([]);
    const [visitData, setVisitData] = useState([]);
    const [surveyData, setSurveyData] = useState([]);

    const imageRef = useRef(null);
    const canvas = document.getElementById('stat-search');



    var chartInstance = useRef(null);
    var osChartInstance = useRef(null);
    const osChartRef = useRef();
    if (chartInstance.current != null) {
        chartInstance.current.destroy();
    }
    useEffect(() => {
        axios.get('http://localhost:8080/get-mouse')
            .then(response => {
                setHeatmapData(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des données:', error);
            });

        axios.get('http://localhost:8080/get-click')
            .then(response => {
                setClickData(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des données:', error);
            });

        axios.get('http://localhost:8080/get-os')
            .then(response => {
                setOsData(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des données:', error);
            });

        axios.get('http://localhost:8080/get-visit')
            .then(response => {
                setVisitData(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des données:', error);
            });

        axios.get('http://localhost:8080/get-survey')
            .then(response => {
                setSurveyData(response.data);
            })
            .catch(error => {
                console.error('Erreur lors de la récupération des données:', error);
            });

    }, [])

    const clickCounts = {};

    clickData.forEach((item) => {
        if (clickCounts[item.element]) {
            clickCounts[item.element] += 1;
        } else {
            clickCounts[item.element] = 1;
        }
    });

    const clickDataForChart = Object.entries(clickCounts).map(([page, count]) => ({
        page,
        count,
    }));

    const chartRef = useRef(null);

    const screenWidth = window.innerWidth;
    const screenHeight = window.innerHeight;

    const minValue = 0;
    const maxValue = Math.max(screenWidth, screenHeight);

    const heatMap = new HeatMap({
        container: document.getElementById('kpi-container'),
        maxOpacity: .6,
        radius: 90,
        blur: 0.6,
    })

    heatMap.setData({
        max: 100,
        min: 2,
        data: []
    })

    heatMapData.forEach((dataPoint) => {
        const { start_x, end_x, start_y, end_y, duration } = dataPoint;

        const x = (start_x + end_x) / 2;
        const y = (start_y + end_y) / 2;

        const value = 30;

        heatMap.addData({ x, y, value });
    })

    heatMap.repaint();



    if (chartInstance.current != null) {
        chartInstance.current.destroy();
    }

    if (canvas instanceof HTMLCanvasElement) {
        const ctx = canvas.getContext('2d');


        const labels = clickDataForChart.map((item) => item.page);
        const total = clickDataForChart.reduce((sum, item) => sum + item.count, 0);
        const data = clickDataForChart.map((item) => {
            const percentage = (item.count / total) * 100;
            return percentage.toFixed(2);
        });

        chartInstance.current = new Chart(ctx, {
            type: 'pie',
            data: {
                labels,
                datasets: [
                    {
                        data,
                        backgroundColor: ['red', 'blue', 'green', 'yellow'],
                    },
                ],
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        labels: {
                            font: {
                                size: 20
                            }
                        }
                    }
                }
            },
        });

    }

    var countByOsType = {};

    osData.forEach(function (item) {
        if (countByOsType[item.osType]) {
            countByOsType[item.osType]++;
        } else {
            countByOsType[item.osType] = 1;
        }
    });

    var totalElements = osData.length;

    var osPercent = {};

    for (var osType in countByOsType) {
        if (countByOsType.hasOwnProperty(osType)) {
            var count = countByOsType[osType];
            var percentage = (count / totalElements) * 100;
            osPercent[osType] = percentage.toFixed(2) + '%';
        }
    }

    var osChartCanvas = document.getElementById('stat-os');


    if (osChartInstance.current != null) {
        osChartInstance.current.destroy();
    }

    if (osChartCanvas instanceof HTMLCanvasElement) {
        var osChartCanvasCtx = osChartCanvas.getContext('2d');

        var osChartLabels = Object.keys(osPercent);
        var osChartData = Object.values(osPercent).map(parseFloat);

        var xAxisLabels = osChartLabels.map(function (label, index) {
            var percentage = osChartData[index].toFixed(2);
            return label + ' (' + percentage + '%)';
        });

        var backgroundColors = ['rgba(255, 255, 255, 0.5)', 'rgba(255, 0, 0, 0.5)', 'rgba(255, 255, 0, 0.2)'];

        osChartInstance.current = new Chart(osChartCanvasCtx, {
            type: 'bar',
            data: {
                labels: xAxisLabels,
                datasets: [{
                    label: 'Pourcentage d\'os utilisés',
                    data: osChartData,
                    backgroundColor: backgroundColors,
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 0.5
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                        max: 100,
                        ticks: {
                            callback: function (value) {
                                return value + '%';
                            }
                        }
                    },
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function (context) {
                                return context.label + ': ' + context.parsed.y + '%';
                            }
                        }
                    }
                }
            }
        });
    }


    function calculateAverageDuration(visits) {
        const pageDurations = new Map();

        for (const visit of visits) {
            const { page, visitDuration } = visit;

            if (!pageDurations.has(page)) {
                pageDurations.set(page, []);
            }

            const durations = pageDurations.get(page);
            durations?.push(visitDuration);
        }

        const averageDurations = new Map();
        for (const [page, durations] of pageDurations.entries()) {
            const totalDuration = durations.reduce((sum, duration) => sum + duration, 0);
            const averageDuration = parseFloat((totalDuration / durations.length).toFixed(2));

            if (averageDuration < 60) {
                averageDurations.set(page, `${averageDuration} sec`);
            } else {
                const averageInMinutes = Math.floor(averageDuration / 60);
                const remainingSeconds = Math.round(averageDuration % 60);
                if (remainingSeconds === 0) {
                    averageDurations.set(page, `${averageInMinutes} minute(s)`);
                } else {
                    averageDurations.set(page, `${averageInMinutes} minute(s) ${remainingSeconds} seconde(s)`);
                }
            }
        }

        return averageDurations;
    }

    const averageDurations = calculateAverageDuration(visitData);

    const data = Array.from(averageDurations.entries()).map(([page, duration]) => {
        return { page, duration };
    });

    const sommeRates = surveyData.reduce((somme, objet) => somme + objet.rate, 0);

    const nombreObjets = surveyData.length;

    const tauxSatisfactionMoyen = ((sommeRates / (nombreObjets * 100)) * 100).toFixed(2);


    return (
        <>
            <h1 className="os-title">Systèmes d'exploitation utilisés :</h1>
            <canvas id="stat-os" ref={osChartRef} className=""></canvas>

            <h1 className="duration-title">Durée moyenne de visite par page :</h1>

            <div className="duration-container">
                {data.map((item) => (
                    <div key={item.page}>
                        <p className="duration-value">{item.duration}</p>
                        <p className="duration-page">Page: {item.page}</p>
                    </div>
                ))}
            </div>

            <div className="satisfaction-container">
                <h1 className="satisfaction-title">Taux de satisfaction :</h1>
                <p className='satisfaction-p'>{tauxSatisfactionMoyen} %</p>
            </div>

            <h1 className="click-title">Taux de clicks par page :</h1>
            <canvas id="stat-search" ref={chartRef} className="searchClickGraph"></canvas>

            {/* <h1 className="heatmap-title">HEATMAP :</h1>
            <div className="kpi-container" id="kpi-container">
                <img
                    ref={imageRef}
                    src="./heatmap-freelance.png"
                    alt="Image"
                />
            </div> */}

        </>

    );
}
function getFromLS(key) {
    let ls = {};
    if (global.localStorage) {
        try {
            ls = JSON.parse(global.localStorage.getItem("rgl-8")) || {};
        } catch (e) { }
    }
    return ls[key];
}

function saveToLS(key, value) {
    if (global.localStorage) {
        global.localStorage.setItem(
            "rgl-8",
            JSON.stringify({
                [key]: value
            })
        );
    }
}

export default Kpi;