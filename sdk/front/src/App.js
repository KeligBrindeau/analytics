import React, { useState } from "react";
import { ThemeProvider, createTheme } from "@mui/material";
import { styled } from '@mui/system';
import { Outlet } from "react-router-dom";


const StyledDrawerHeader = styled('div')(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: "flex-end"
}));

export default function App() {

  const [darkMode, setDarkMode] = useState(true);

  const theme = createTheme({
    palette: {
      mode: darkMode ? "dark" : "light"
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <StyledDrawerHeader />

      <Outlet />
    </ThemeProvider>
  );
}