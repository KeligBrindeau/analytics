import React from 'react';
import './register.css';
import axios from 'axios';

const Register = props => (
    <LoginForm />
);



class LoginForm extends React.Component {
  render() {
    return (
      <div id="loginform">
        <FormHeader title="Register" />
        <Form />

      </div>
    )
  }
}

const FormHeader = props => (
  <h2 id="headerTitle">{props.title}</h2>
);

const Form = props => (
  <div>
    <form onSubmit={onSubmitHandler}>
      <FormInput description="Email" placeholder="email" type="text" />
      <FormInput description="Password" placeholder="password" type="password" />
      <FormInput description="Kbis" placeholder="kbis" type="text" />
      <FormInput description="Company" placeholder="company" type="text" />
      <FormInput description="Contact" placeholder="contact" type="text" />
      <FormInput description="Website" placeholder="website" type="text" />
      <FormButton title="Register" />
    </form>
  </div>
);

const FormButton = props => (
  <div id="button" className="row">
    <button type='submit'>{props.title}</button>
  </div>
);

const FormInput = props => (
  <div className="row">
    <label>{props.description}</label>
    <input required type={props.type} placeholder={props.placeholder} id={props.placeholder} />
  </div>
);

const onSubmitHandler = async (e: React.FormEvent) => {
  e.preventDefault();
  console.log('submit');
  const email = document.getElementById('email').value;
  const password = document.getElementById('password').value;
  const kbis = document.getElementById('kbis').value;
  const company = document.getElementById('company').value;
  const contact = document.getElementById('contact').value;
  const website = document.getElementById('website').value;

  console.log(JSON.stringify({ email, password, kbis, company, contact, website }));
  const response = await axios.post('http://localhost:8080/users/register', JSON.stringify({ email, password, kbis, company, contact, website }), {
    headers: {
      'Content-Type': 'application/json'
    }
  });
  console.log(response);

  if (response.data.status === 201) {
    console.log('register success');
    window.location.href = '/login';
  } else {
    console.log('register failed');
    document.getElementById('loginform').innerHTML += "<div className='row'>Register failed</div>";
  }



};


export default Register;