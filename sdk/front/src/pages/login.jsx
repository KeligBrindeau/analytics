import React from 'react';
import './register.css';
import axios from 'axios';

const Login = props => (
  <LoginForm />
);


class LoginForm extends React.Component{
render(){
  return(
    <div id="loginform">
      <FormHeader title="Login" />
      <Form />
      
    </div>
  )
}
}

const FormHeader = props => (
  <h2 id="headerTitle">{props.title}</h2>
);

const Form = props => (
 <div>
    <form onSubmit={onSubmitHandler}>
      <FormInput description="Username" placeholder="email" type="text" />
      <FormInput description="Password" placeholder="password" type="password"/>
      <FormButton title="Log in"/>
    </form>
 </div>
);

const FormButton = props => (
<div id="button" className="row">
  <button type='submit'>{props.title}</button>
</div>
);

const FormInput = props => (
<div className="row">
  <label>{props.description}</label>
  <input type={props.type} placeholder={props.placeholder} id={props.placeholder}/>
</div>  
);

const onSubmitHandler = async (e: React.FormEvent) => {
  e.preventDefault();
  console.log('submit');
  const email = document.getElementById('email').value;
  const password = document.getElementById('password').value;

  const response = await axios.post('http://localhost:8080/users/login', JSON.stringify({ email, password }), {
    headers: {
      'Content-Type': 'application/json'
    }
  });
  if (response.data.status === 200) {
    console.log('login success');
    window.location.href = '/';
  }else{
    console.log('login failed');
    document.getElementById('loginform').innerHTML += "<div className='row'>login failed</div>";
  }
  console.log(response);



};


export default Login;