# Télécharger le sdk dans un projet

Lien npm du sdk : https://www.npmjs.com/package/sdk-analytics

Lancer la commande suivante :

```
npm i sdk-analytics
```

Au moment de l'installation du package, la plateforme analytics s'ouvrira automatiquement dans le navigateur. 
La plateforme du sdk utilise le port 3000, assurez vous qu'il soit disponible.

# API

Pour lancer l'api, exécutez la commande suivante à la racine de ce projet que vous venez de cloner:

```
docker compose up -d
```

L'api a une base de données Postgres et Mongo. La db postgres est pour l'authentification, et la db mongo pour les données d'analytics.

# Utilisation du SDK

## Mouse Tracking

Pour appeler la fonction de mouse tracking dans votre code, utilisez l'import suivant :

```
import { handleMouseEvent } from 'sdk-analytics';
```

Cette fonction attend deux arguments :

1 - le document, car c'est sur cet élement que sera ajouté l'event listener

2 - la page (type string)

### Exemple d'utilisation

```
handleMouseEvent(document, "home");
```

## Click

Notre SDK permet également de tracker les clicks sur n'importe quel élément de votre projet. Pour appeler la fonction, utilisez l'import suivant :

```
import { handleClick } from 'sdk-analytics';
```

Cette fonction attend trois arguments : 

1 - L'élément sur lequel vous voulez ajouter l'event listener

2 - la page (type string) sur laquelle se situe l'élément

3 - l'id de l'élément, afin de l'identifier

### Exemple d'utilisation

```
const el = document.getElementById("test");
handleClick(el, "home", el.id)
```

## Temps de visite

Cette fonctionnalité va vous permettre de calculer le temps de visite moyen des utilisateurs sur une page. 
En cas d'inactivité de l'utilisateur, la fin de la visite est enregistrée.

Pour appeler la fonction, utilisez l'import suivant :

```
import { setVisitListener } from 'sdk-analytics';
```

Cette fonction attend trois arguments :

1 - le document, car c'est sur cet élement que sera ajouté l'event listener

2 - l'élément window de la page, car un event listener sera ajouté à la fenêtre du navigateur

3 - la page (type string) sur laquelle se situe l'élément

### Exemple d'utilisation

```
setVisitListener(document, window, "home");
```

## Enquête de satisfaction

Vous pouvez intégrer à votre site une enquête de satisfaction. Après avoir créé votre formulaire, importez la fonction suivante :

```
import { handleSurvey } from 'sdk-analytics';
```

Cette fonction devra être appelée au moment de la soumission du formulaire. Elle attend un argument :

1 - la note saisie dans le formulaire

### Exemple d'utilisation

Cette variable va contenir la note saisie dans le formulaire
```
const [ratingValue, setRatingValue] = useState(null)

function handleSubmit = () => {
    handleSurvey(ratingValue)
}
```

## Système d'exploitation

Vous allez pouvoir comparer les systèmes d'exploitations de vos utilisateurs. Pour appeler la fonction, importez la fonction suivante :

```
import { handleOs } from 'sdk-analytics';
```

Cette fonction n'attend pas d'argument.

### Exemple d'utilisation

```
handleOs()
```