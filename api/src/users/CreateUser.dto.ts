import { IsDefined, IsString, IsStrongPassword, IsUUID, MaxLength } from "class-validator";

export class CreateUserDto {
    public id: string; 
    @IsString()
    public email: string;
    @IsString()
    public password: string;
    @IsString()
    public website: string;
    @IsString()
    public contact: string;
    @IsString()
    public kbis: string;
    @IsString()
    public company: string;
}