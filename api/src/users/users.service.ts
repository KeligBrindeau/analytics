import { Body, HttpStatus, Injectable, ValidationPipe } from '@nestjs/common';
import { UserEntity } from './users.entity';
import { CreateUserDto } from './CreateUser.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LoginUserDto } from './LoginUser.dto';
import * as bcrypt from 'bcryptjs';
import { AuthPayload } from './auth-payload.interface';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class UsersService {

    public constructor(@InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        private readonly jwtService: JwtService,
    ) { }

    //register
    async register(@Body(ValidationPipe) CreateUserDto: CreateUserDto) {

        const emailExist = await this.searchUser(CreateUserDto.email)
        if (emailExist) {
            return { status: HttpStatus.BAD_REQUEST, message: 'Cette adresse mail est déjà utilisée.' };
        }

        var hash = bcrypt.hashSync(CreateUserDto.password, bcrypt.genSaltSync(10));

        const user = {
            ...CreateUserDto,
            password: hash
        };
        this.userRepository.insert(user)
        return { status: HttpStatus.CREATED, message: 'Register ok' };
    }

    async generateJwtToken(payload: AuthPayload): Promise<string> {
        const jwt = this.jwtService.signAsync(payload, {
            expiresIn: "24h",
            secret: "jwt_secret",
        },);
        return jwt
    }

    //login
    async login(@Body(ValidationPipe) LoginUserDto: LoginUserDto) {
        const user = await this.searchUser(LoginUserDto.email)
        if (!user) {
            return { status: HttpStatus.BAD_REQUEST, message: 'Cette adresse mail n\'existe pas.' };
        }

        const match = bcrypt.compareSync(LoginUserDto.password, user.password);
        if (!match) {
            return { status: HttpStatus.BAD_REQUEST, message: 'Mot de passe incorrect.' };
        }

        const payload: AuthPayload = { email: user.email };
        const jwt = await this.generateJwtToken(payload);
        console.log("jwt " + jwt);
        let result = {};
        try {
            const token: string = await this.generateJwtToken(payload);
            console.log("GENERATED TOKEN " + token)
            const newToken = user
            newToken.token = token;
            await this.userRepository.update(user.id, newToken);
            result = {
                status: HttpStatus.CREATED,
                message: 'token_create_success',
                token: token
            };
        } catch (e) {
            result = {
                status: HttpStatus.BAD_REQUEST,
                message: 'token_create_bad_request',
                token: null
            };
        }

        return { status: HttpStatus.OK, message: 'Connexion réussie.' };
    }

    public searchUser(email: string) {
        return this.userRepository.findOne({ where: { email: email } });
    }


}
