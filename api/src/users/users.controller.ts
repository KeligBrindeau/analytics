import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { Get } from '@nestjs/common';
import { CreateUserDto } from './CreateUser.dto';
import { UsersService } from './users.service';
import { LoginUserDto } from './LoginUser.dto';

@Controller('users')
export class UsersController {

    public constructor(private readonly userService: UsersService) { }

    // /test
    @Get('test')
    test() {
        return 'test';
    }

    @Post('register')
    register(@Body(ValidationPipe) createPostDto: CreateUserDto) {
        return this.userService.register(createPostDto);
    }

    @Post('login')
    login(@Body(ValidationPipe) LoginUserDto: LoginUserDto) {
        return this.userService.login(LoginUserDto);
    }
}
