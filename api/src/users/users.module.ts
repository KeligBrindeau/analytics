import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './users.entity';
import { JwtModule } from '@nestjs/jwt';
// import { jwtConstants } from './constants';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    JwtModule.register(
      { secret: "JWT_SECRET"}
    ),

  ],
  controllers: [UsersController],
  providers: [UsersService]
})
export class UsersModule { }

