import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    BeforeInsert,
} from 'typeorm';

import { hash } from 'bcryptjs';

@Entity('users')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        type: 'text',
        unique: true,
        nullable: false
    })
    email: string;

    @Column({
        type: 'text',
        nullable: false
    })
    password: string;

    @Column({
        type: String,
        length: 255,
        nullable: false
    })
    website: string;

    @Column({
        type: String,
        length: 255,
        nullable: false
    })
    contact: string;

    @Column({
        type: String,
        length: 255,
        nullable: true
    })
    kbis: string;

    @Column({
        type: String,
        length: 255,
        nullable: true
    })
    company: string;

    @Column({
        type: String,
        length: 255,
        nullable: true
    })
    token: string;

    @CreateDateColumn()
    createdOn: Date;

    @BeforeInsert()
    hashPassword = async () => {
        this.password = await hash(this.password, 8);
    };
}

type SanitizeUserOptions = {
    withToken?: boolean;
};
