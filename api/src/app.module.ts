import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'postgres',
      port: 5432,
      username : 'postgres',
      password : 'postgres',
      database : 'postgres',
      autoLoadEntities: true,
      synchronize: true,
    }), 
    MongooseModule.forRoot('mongodb://root:password@mongo:27017/?authMechanism=DEFAULT')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
