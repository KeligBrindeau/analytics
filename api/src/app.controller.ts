import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/list-analytics')
  async getAnalytics() {
    return await this.appService.getAnalytics();
  }

  @Post('/save-click')
  saveCLick(@Body() click){
    return this.appService.saveClick(click);
  }

  @Get('/get-click')
  async getClick(){
    return await this.appService.getClick();
  }

  @Post('/save-os')
  async saveOs(@Body() os){
    return await this.appService.saveOs(os);
  }

  @Get('/get-os')
  async getOs(){
    return await this.appService.getOs();
  }

  @Post('/save-survey')
  saveSurvey(@Body() survey){
    return this.appService.saveSurvey(survey);
  }

  @Get('/get-survey')
  async getSurvey(){
    return await this.appService.getSurveyData();
  }

  @Post('/save-visit')
  saveVisit(@Body() visit){
    return this.appService.saveVisit(visit);
  }

  @Get('/get-visit')
  getVisit(){
    return this.appService.getVisit();
  }

  @Post('/save-mouse')
  saveMouse(@Body() mouse){
    return this.appService.saveMouse(mouse);
  }

  @Get('/get-mouse')
  getMouse(){
    return this.appService.getMouse();
  }


}
