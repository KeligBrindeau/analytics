import { Injectable } from '@nestjs/common';
import { MongoClient } from 'mongodb';

@Injectable()
export class AppService {
  private readonly dbName = "kpi";
  private readonly collectionName = "analytics";
  private readonly clickCollection = "click";
  private readonly osCollection = "os";
  private readonly surveyCollection = "survey";
  private readonly visitCollection = "visit";
  private readonly mouseCollection = "mouse";
  private readonly uri = "mongodb://root:password@mongo:27017/?authMechanism=DEFAULT";

  getHello(): string {
    return 'Hello World!';
  }

  async getAnalytics(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.collectionName);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }

  async saveClick(click: any): Promise<any> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.clickCollection);

    const result = await collection.insertOne(click);

    client.close();

    return result;
  }

  async getClick(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.clickCollection);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }

  async saveOs(os: any): Promise<any> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.osCollection);

    const result = await collection.insertOne(os);

    client.close();

    return result;
  }

  async getOs(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.osCollection);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }

  async saveSurvey(survey: any): Promise<any> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.surveyCollection);

    const result = await collection.insertOne(survey);

    client.close();

    return result;
  }

  async getSurveyData(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.surveyCollection);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }

  async saveVisit(visit: any): Promise<any> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.visitCollection);

    const result = await collection.insertOne(visit);

    client.close();

    return result;
  }

  async getVisit(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.visitCollection);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }

  async saveMouse(mouse: any): Promise<any> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.mouseCollection);

    const result = await collection.insertOne(mouse);

    client.close();

    return result;
  }

  async getMouse(): Promise<any[]> {
    const client = new MongoClient(this.uri);
    await client.connect();

    const db = client.db(this.dbName);
    const collection = db.collection(this.mouseCollection);

    const result = await collection.find().toArray();

    client.close();

    return result;
  }
}