-- Create default database used by the application
SELECT 'CREATE DATABASE authentification'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'authentification')\gexec

GRANT ALL PRIVILEGES ON DATABASE authentification TO postgres;